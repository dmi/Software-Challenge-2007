object Form_Server: TForm_Server
  Left = 0
  Top = 0
  Width = 807
  Height = 512
  Caption = 'Form_Server'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object SG_GameBoard: TStringGrid
    Left = 440
    Top = 0
    Width = 321
    Height = 233
    DefaultColWidth = 24
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
    TabOrder = 0
    Visible = False
    ColWidths = (
      24
      24
      24
      24
      24)
    RowHeights = (
      24
      24
      24
      24
      24)
  end
  object GB_Entwicklung: TGroupBox
    Left = 440
    Top = 248
    Width = 313
    Height = 185
    Caption = 'Entwickleransicht'
    TabOrder = 1
    object La_Ziel: TLabel
      Left = 15
      Top = 36
      Width = 33
      Height = 13
      Caption = 'La_Ziel'
    end
    object La_PinguinNummer: TLabel
      Left = 210
      Top = 100
      Width = 90
      Height = 13
      Caption = 'La_PinguinNummer'
    end
    object La_Path: TLabel
      Left = 15
      Top = 76
      Width = 39
      Height = 13
      Caption = 'La_Path'
    end
    object La_Ausgang: TLabel
      Left = 15
      Top = 20
      Width = 59
      Height = 13
      Caption = 'La_Ausgang'
    end
    object La_CheckPath: TLabel
      Left = 15
      Top = 60
      Width = 68
      Height = 13
      Caption = 'La_CheckPath'
    end
    object La_Direction: TLabel
      Left = 216
      Top = 44
      Width = 46
      Height = 13
      Caption = 'Richtung:'
    end
    object La_Movable: TLabel
      Left = 210
      Top = 112
      Width = 57
      Height = 13
      Caption = 'La_Movable'
    end
    object La_Position: TLabel
      Left = 210
      Top = 88
      Width = 54
      Height = 13
      Caption = 'La_Position'
    end
    object La_Fish: TLabel
      Left = 210
      Top = 124
      Width = 36
      Height = 13
      Caption = 'La_Fish'
    end
    object La_GetAllFish: TLabel
      Left = 210
      Top = 136
      Width = 64
      Height = 13
      Caption = 'La_GetAllFish'
    end
    object Btn_TargetsInDirection: TButton
      Left = 208
      Top = 16
      Width = 97
      Height = 25
      Caption = 'TargetsInDirection'
      TabOrder = 0
    end
    object SE_Direction: TSpinEdit
      Left = 264
      Top = 40
      Width = 41
      Height = 22
      MaxValue = 5
      MinValue = 0
      TabOrder = 1
      Value = 0
    end
    object Btn_GetAllTargets: TButton
      Left = 208
      Top = 64
      Width = 97
      Height = 17
      Caption = 'GetAllTargets'
      TabOrder = 2
    end
    object Btn_Encode: TButton
      Left = 16
      Top = 96
      Width = 75
      Height = 25
      Caption = 'EncodeBoard'
      TabOrder = 3
    end
    object Btn_Renumber: TButton
      Left = 16
      Top = 128
      Width = 97
      Height = 17
      Caption = 'RenumberPinguins'
      TabOrder = 4
    end
  end
  object GB_Spiel: TGroupBox
    Left = 24
    Top = 368
    Width = 265
    Height = 73
    Caption = 'Spiel'
    TabOrder = 2
    object La_Player: TLabel
      Left = 15
      Top = 20
      Width = 47
      Height = 18
      Caption = 'Spieler'
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object La_Turn: TLabel
      Left = 15
      Top = 44
      Width = 42
      Height = 18
      Caption = 'Runde'
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object La_Score_0: TLabel
      Left = 120
      Top = 25
      Width = 81
      Height = 13
      Caption = 'Punkte Spieler 0:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object La_Score_1: TLabel
      Left = 120
      Top = 49
      Width = 81
      Height = 13
      Caption = 'Punkte Spieler 1:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
  end
  object GB_GameShow: TGroupBox
    Left = 296
    Top = 368
    Width = 113
    Height = 73
    TabOrder = 3
    Visible = False
    object Btn_Next: TButton
      Left = 56
      Top = 15
      Width = 17
      Height = 17
      Caption = '>'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Btn_Previous: TButton
      Left = 40
      Top = 15
      Width = 17
      Height = 17
      Caption = '<'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object Btn_First: TButton
      Left = 8
      Top = 15
      Width = 25
      Height = 17
      Caption = '<<'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object Btn_Last: TButton
      Left = 80
      Top = 15
      Width = 25
      Height = 17
      Caption = '>>'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object Btn_Auto: TButton
      Left = 32
      Top = 42
      Width = 49
      Height = 21
      Caption = 'play'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
  end
  object MainMenu: TMainMenu
    object MM_Datei: TMenuItem
      Caption = 'Datei'
      object Spielladen1: TMenuItem
        Caption = #214'ffnen'
      end
      object MM_Save: TMenuItem
        Caption = 'Speichern'
        ShortCut = 16467
      end
      object MM_Close: TMenuItem
        Caption = 'Beenden'
      end
    end
    object MM_Ansicht: TMenuItem
      Caption = 'Ansicht'
      object MM_Entwicklung: TMenuItem
        AutoCheck = True
        Caption = 'Entwickleransicht'
        Checked = True
      end
      object MM_Hex: TMenuItem
        AutoCheck = True
        Caption = 'Hexagonalfeld'
        Checked = True
        Enabled = False
      end
    end
    object MM_Game: TMenuItem
      Caption = 'Spiel'
      object MM_Undo: TMenuItem
        Caption = 'R'#252'ckg'#228'ngig'
        Enabled = False
        ShortCut = 16474
      end
      object MM_NewGame: TMenuItem
        Caption = 'Neues Spiel'
        ShortCut = 16462
      end
      object MM_Options: TMenuItem
        Caption = 'Einstellungen'
      end
    end
    object MM_MultiGame: TMenuItem
      Caption = 'Testreihen'
      Enabled = False
    end
    object MM_Info: TMenuItem
      Caption = '?'
      object MM_About: TMenuItem
        Caption = #220'ber'
      end
    end
  end
  object Timer_Refresh: TTimer
    Enabled = False
    Interval = 10
    Left = 65535
    Top = 65528
  end
  object OpenDialog_Game: TOpenDialog
    Filter = 'Packeis am Pol - Spiel (*.pap)|*.pap|Alle Dateien|*'
    Title = 'Packeis am Pol - Spiel '#246'ffnen'
  end
  object SaveDialog_Game: TSaveDialog
    DefaultExt = 'pap'
    Filter = 'Packeis am Pol - Spiel (*.pap)|*.pap|Alle Dateien|*'
    Title = 'Packeis am Pol - Spiel speichern'
  end
  object Timer_GameShow: TTimer
    Interval = 1600
  end
end
