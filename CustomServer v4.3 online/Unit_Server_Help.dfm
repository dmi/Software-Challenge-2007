object Form_Help: TForm_Help
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Packeis am Pol - Hilfe'
  ClientHeight = 326
  ClientWidth = 396
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object La_Title: TLabel
    Left = 184
    Top = 8
    Width = 38
    Height = 19
    Caption = 'Hilfe'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object La_Selection: TLabel
    Left = 184
    Top = 32
    Width = 36
    Height = 18
    Alignment = taCenter
    Caption = 'Hilfe'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Btn_OK: TButton
    Left = 288
    Top = 280
    Width = 89
    Height = 33
    Caption = 'OK'
    TabOrder = 0
    OnClick = Btn_OKClick
  end
  object RG_Selection: TRadioGroup
    Left = 8
    Top = 264
    Width = 121
    Height = 57
    Caption = 'Auswahl'
    ItemIndex = 0
    Items.Strings = (
      'Spiel'
      'Entwickleransicht'
      'Clients')
    TabOrder = 1
    OnClick = RG_SelectionClick
  end
  object RichEdit_Spiel: TRichEdit
    Left = 8
    Top = 56
    Width = 377
    Height = 201
    BevelInner = bvNone
    BiDiMode = bdLeftToRight
    Color = clBtnFace
    Lines.Strings = (
      'Aufbau: Das Spielfeld besteht aus 100 zuf'#228'llig angeordneten '
      
        'hexagonalen Feldern, die unterschiedliche Wertigkeiten, also Fis' +
        'che, '
      'besitzen:'
      '30 1-Fisch-Felder;  20 2-Fisch-Felder;  10 3-Fisch-Felder'
      
        'Zwei Spieler besitzen jeweils vier Pinguine, die sie auf den Fel' +
        'dern '
      'bewegen k'#246'nnen.'
      ''
      
        'Ablauf: Die beiden Spieler setzen/bewegen immer abwechselnd in j' +
        'eder '
      'Runde einen Pinguin.'
      
        'Zu Beginn des Spiels werden alle Pinguine auf Felder mit einem F' +
        'isch '
      'gesetzt.'
      
        'Danach werden sie '#252'ber die Felder bewegt, wobei sie nur in sechs' +
        ' '
      
        'verschiedene Richtungen in einer geraden Linie gezogen werden d'#252 +
        'rfen. '
      
        'Die Pinguine k'#246'nnen nur '#252'ber unbesetzte Schollen gehen. Nicht '#252'b' +
        'er '
      'Wasser und auch nicht '#252'ber andere Pinguine.'
      
        'Wenn ein Pinguin gezogen wurde, erh'#228'lt der Spieler desselben die' +
        ' Anzahl '
      
        'der Fische der Ausgangsscholle des gezogenen Pinguins. Dieses Fe' +
        'ld geht '
      'daraufhin unter.'
      ''
      
        'Ausgang: Das Spiel endet, wenn alle Pinguine bewegungsunf'#228'hig si' +
        'nd.'
      
        'Es hat der Spieler gewonnen, der die meisten Fische eingesammelt' +
        ' hat '
      '(zuz'#252'glich derjenigen, auf denen die Pinguine am Ende stehen).'
      
        'Herrscht dort Gleichstand, gibt die Anzahl der "eingesammelten" ' +
        'Schollen '
      'den Ausschlag.')
    ParentBiDiMode = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 2
  end
  object RichEdit_Entwickleransicht: TRichEdit
    Left = 8
    Top = 56
    Width = 377
    Height = 201
    BevelInner = bvNone
    BiDiMode = bdLeftToRight
    Color = clBtnFace
    Lines.Strings = (
      
        'Die Entwickleransicht dient dazu, einen tieferen Einblick in die' +
        ' '
      'Rechenabl'#228'ufe im Hintergrund des Programms zu erhalten.'
      ''
      
        'So wird in der Tabelle das Spielfeld in der Matrix dargestellt, ' +
        'mit der das '
      
        'Programm arbeitet. Eine Behandlung von hexagonalen Feldern ist i' +
        'n '
      'diesem Sinne vom Programm her nicht m'#246'glich.'
      
        'Es wird deswegen mit einem 2-dimensionalen Array (die Feldmatrix' +
        ') '
      
        'gearbeitet. In dieser Matrix sind alle Felder in vertikaler, hor' +
        'izontaler und '
      
        'diagonaler (nur von links unten nach rechts oben) Richtung errei' +
        'chbar - '
      'also sechs Richtungen.'
      
        'In der Tabelle bedeuten die Werte auf g'#252'ltigen Schollen die Fisc' +
        'hanzahl. '
      
        'Auf besetzten gibt "P1", bzw "P2" an, welchem Spieler der Pingui' +
        'n geh'#246'rt. '
      
        'Die Nummer hinter dem Komma ist die interne Nummer des Pinguins,' +
        ' '#252'ber '
      'die im Programm auf diesen zugegriffen wird.'
      ''
      
        'Neben der Matrix bietet die Entwickleransicht auch die M'#246'glichke' +
        'it, die '
      
        'visuelle Oberfl'#228'che zu aktualiesieren und eine Auskunft '#252'ber ein' +
        'ige Werte '
      'der Mausbewegung.'
      'Zudem werden einige Informationen '#252'ber den ausgew'#228'hlten Pinguin '
      'angezeigt.')
    ParentBiDiMode = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object RichEdit_Clients: TRichEdit
    Left = 8
    Top = 56
    Width = 377
    Height = 201
    BevelInner = bvNone
    BiDiMode = bdLeftToRight
    Color = clBtnFace
    Lines.Strings = (
      
        'Die "Clients" sind externe Programme, die Z'#252'ge f'#252'r die Pinguine ' +
        'eines '
      'Spielers ausgeben - eine k'#252'nstliche Intelligenz.'
      'Diese Programme sind Konsolenanwendungen, die der Server im '
      
        'Hintergrund startet und ihnen jede Runde das Spielfeld in kodier' +
        'ter Form '
      #252'bermittelt. Im Gegenzug schreiben sie einen Zug in das '
      '"Konsolenfenster".'
      ''
      
        'Es k'#246'nnen nicht nur Z'#252'ge, sondern auch Debugnachrichten ausgegeb' +
        'en '
      
        'werden. Diese sollten sicherheitshalber mit dem Schl'#252'sselwort "d' +
        'ebug" '
      'beginnen.'
      ''
      'Achtung: Wenn der Server abst'#252'rzt, ohne die Clients beenden zu '
      
        'k'#246'nnen, laufen sie als Prozesse im Hintergrund weiter. Deshalb s' +
        'ollte bei '
      
        'einem Absturz im Taskmanager '#252'berpr'#252'ft werden, ob noch Clients l' +
        'aufen. '
      'Diese k'#246'nnen dann mit "Prozess beenden" geschlossen werden.')
    ParentBiDiMode = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 4
  end
end
