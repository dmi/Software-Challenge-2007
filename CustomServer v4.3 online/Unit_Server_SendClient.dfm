object Form_ClientSend: TForm_ClientSend
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Sende an Client'
  ClientHeight = 60
  ClientWidth = 401
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Ed_Send: TEdit
    Left = 8
    Top = 8
    Width = 385
    Height = 21
    TabOrder = 0
    OnKeyUp = Ed_SendKeyUp
  end
  object Btn_OK: TButton
    Left = 160
    Top = 32
    Width = 89
    Height = 25
    Caption = 'OK'
    TabOrder = 1
    OnClick = Btn_OKClick
  end
end
