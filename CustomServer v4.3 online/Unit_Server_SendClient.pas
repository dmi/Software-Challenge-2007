unit Unit_Server_SendClient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm_ClientSend = class(TForm)
    Ed_Send: TEdit;
    Btn_OK: TButton;
    procedure FormShow(Sender: TObject);
    procedure Ed_SendKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn_OKClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_ClientSend: TForm_ClientSend;

implementation

uses Unit_Server_Main;

{$R *.dfm}

procedure TForm_ClientSend.Btn_OKClick(Sender: TObject);
begin
  Board.Player[Board.Currentplayer].SendToClient(Ed_Send.Text);
  close;
end;

procedure TForm_ClientSend.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Form_Server.Enabled:=TRUE;
end;

procedure TForm_ClientSend.Ed_SendKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  IF Key=13 THEN Btn_OK.Click;
end;

procedure TForm_ClientSend.FormShow(Sender: TObject);
begin
  Ed_Send.SetFocus;
  Ed_Send.SelectAll;
end;

end.
