unit Unit_Timer;

interface
uses windows,mmsystem ;

type
  TMyTimer = class
    private
      StartTime: Integer;
    public
      procedure ResetTime;
      function CheckTime: Integer;
  end;



implementation


function TMyTimer.CheckTime: Integer; //Die verstrichene Zeit in Millisekunden
begin
  result:=GetTickCount-self.StartTime;
end;

procedure TMyTimer.ResetTime; //Setzt die Zeit zur�ck
begin
  self.StartTime:=GetTickCount;
end;


end.
